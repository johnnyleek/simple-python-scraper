import requests, sys, json
from auth import credentials

class Scraper():

    def __init__(self):
        self.login_info = {'username': credentials['username'], 'password': credentials['password']}
        self.SITE_ROOT = 'http://localhost:8000'
        self.TOTAL_LOGIN_ATTEMPTS = 0

    def attempt_login(self, session, request):
        if self.TOTAL_LOGIN_ATTEMPTS > 10:
            print("Error: Too many login attempts. Server down? Quitting.")
            sys.exit(0)

        print("Server Error 500, attempting new login...")
        self.TOTAL_LOGIN_ATTEMPTS += 1
        return session.post('{site_root}/helm/login/?next=/helm/changecontrol/change/'.format(site_root=self.SITE_ROOT), data=self.login_info)

    def scrape(self):
        with requests.Session() as session:
            # Send initial request to get the csrf token and add it to the payload
            # The token is stored as a cookie in the session
            print("Fetching CSRF Token...")
            csrf_request = session.get('{site_root}/helm/login/'.format(site_root=self.SITE_ROOT))
            self.login_info['csrfmiddlewaretoken'] = session.cookies['csrftoken']

            # Send login request using provided credentials and csrf token
            print("Attempting login...")
            login = session.post('{site_root}/helm/login/?next=/helm/changecontrol/change/'.format(site_root=self.SITE_ROOT), data=self.login_info)
            self.TOTAL_LOGIN_ATTEMPTS += 1
            # If there is a server error, keep attempting login (it happens sometimes)
            if login.status_code == 500:
                attempt_login(session, login)
            print("Logged in successfully!")

            # Create a dictionary containing the data we want to post
            new_data = { 'server': 'aux-resweb1', 'summary': 'Testing a python request' }

            # Set the headers of the request (specify it's a JSON request, and send the csrftoken)
            headers = {'Content-type': 'application/json', 'X-CSRFToken': session.cookies['csrftoken']}

            # Post the new data
            post_new = session.post('{site_root}/api/deploy/create_change/'.format(site_root=self.SITE_ROOT), json=new_data, headers=headers)

            # If API returns 200, good request
            if post_new.status_code == 200:
                print("Posted to API Successfully!")

            # If the API returns anything other than 200, write the response to a file
            if not post_new.status_code == 200:
                print("Request failed... Server response in \"output\"")
                with open('output', 'w') as outfile:
                    outfile.write(post_new.text)
                    outfile.close()
scraper = Scraper()
scraper.scrape()
